from __future__ import division
from __future__ import print_function

import random
import string
import sys
import argparse
try:
    import ConfigParser
except ImportError:
    import configparser as ConfigParser
import logging
from impacket.examples import logger
from impacket import version
from impacket.krb5.keytab import Keytab
from impacket.dcerpc.v5 import transport, srvs, scmr
CODEC = sys.stdout.encoding

class LIEXEC:
    def __init__(self, username='', password='', domain='', hashes=None, aesKey=None,
                 doKerberos=None, kdcHost=None, port=445, cmd=''):

        self.__username = username
        self.__password = password
        self.__port = port
        self.__serviceName = ''.join([random.choice(string.ascii_letters) for i in range(5)])
        self.__domain = domain
        self.__lmhash = ''
        self.__nthash = ''
        self.__aesKey = aesKey
        self.__doKerberos = doKerberos
        self.__kdcHost = kdcHost
        self.__cmd = cmd
        self.shell = None
        if hashes is not None:
            self.__lmhash, self.__nthash = hashes.split(':')

    def run(self, remoteName, remoteHost):
        stringbinding = r'ncacn_np:%s[\pipe\svcctl]' % remoteName
        logging.debug('StringBinding %s'%stringbinding)
        rpctransport = transport.DCERPCTransportFactory(stringbinding)
        rpctransport.set_dport(self.__port)
        rpctransport.setRemoteHost(remoteHost)
        if hasattr(rpctransport, 'set_credentials'):
            # This method exists only for selected protocol sequences.
            rpctransport.set_credentials(self.__username, self.__password, self.__domain, self.__lmhash,
                                         self.__nthash, self.__aesKey)
        rpctransport.set_kerberos(self.__doKerberos, self.__kdcHost)

        self.shell = None
        try:
            rpcsvc = rpctransport.get_dce_rpc()
            rpcsvc.connect()
            rpcsvc.bind(scmr.MSRPC_UUID_SCMR)
            svcHandle = None
            try:
                rsps = scmr.hROpenSCManagerW(rpcsvc)
                svcHandle = rsps['lpScHandle']
                try:
                    rsps = scmr.hROpenServiceW(rpcsvc, svcHandle, self.__serviceName + '\x00')
                except Exception as e:
                    if str(e).find('ERROR_SERVICE_DOES_NOT_EXIST') == -1:
                        raise e  # Unexpected error
                else:
                    scmr.hRDeleteService(rpcsvc, rsps['lpServiceHandle'])
                    scmr.hRCloseServiceHandle(rpcsvc, rsps['lpServiceHandle'])
                print('[+] Creating service %s.....' % self.__serviceName)
                rsps = scmr.hRCreateServiceW(rpcsvc, svcHandle, self.__serviceName + '\x00',
                                             self.__serviceName + '\x00',
                                             lpBinaryPathName="%COMSPEC% /Q /c " + self.__cmd + '\x00') # or cmd.exe?
                serviceHandle = rsps['lpServiceHandle']
                if serviceHandle:
                    try:
                        print('[+] Starting service %s.....' % self.__serviceName)
                        scmr.hRStartServiceW(rpcsvc, serviceHandle)
                    except Exception as e:
                        print(str(e))

                    print('[!] Removing service %s.....' % self.__serviceName)
                    scmr.hRDeleteService(rpcsvc, serviceHandle)
                    scmr.hRCloseServiceHandle(rpcsvc, serviceHandle)
            except Exception as e:
                print("[-] ServiceExec Error on: %s" % rpctransport.get_remote_host())
                print(str(e))
            finally:
                if svcHandle:
                    scmr.hRCloseServiceHandle(rpcsvc, svcHandle)

            rpcsvc.disconnect()

            # self.shell = RemoteShell(self.__share, rpctransport, self.__mode, self.__serviceName) # exec service
        except (Exception, KeyboardInterrupt) as e:
            if logging.getLogger().level == logging.DEBUG:
                import traceback
                traceback.print_exc()
            logging.critical(str(e))
           # if self.shell is not None:
            #    self.shell.finish()
            sys.stdout.flush()
            sys.exit(1)

if __name__ == '__main__':
    print(version.BANNER)
    print("[*] LiExec - no revshell - nch.ninja")
    parser = argparse.ArgumentParser()
    parser.add_argument('target', action='store', help='[[domain/]username[:password]@]<targetName or address>')
    parser.add_argument('-ts', action='store_true', help='adds timestamp to every logging output')
    parser.add_argument('-debug', action='store_true', help='Turn DEBUG output ON')
    parser.add_argument('-codec', action='store', help='Sets encoding used (codec) from the target\'s output (default '
                                                       '"%s"). If errors are detected, run chcp.com at the target, '
                                                       'map the result with '
                          'https://docs.python.org/3/library/codecs.html#standard-encodings and then execute smbexec.py '
                          'again with -codec and the corresponding codec ' % CODEC)

    group = parser.add_argument_group('connection')

    group.add_argument('-dc-ip', action='store',metavar = "ip address", help='IP Address of the domain controller. '
                       'If omitted it will use the domain part (FQDN) specified in the target parameter')
    group.add_argument('-target-ip', action='store', metavar="ip address", help='IP Address of the target machine. If '
                       'ommited it will use whatever was specified as target. This is useful when target is the NetBIOS '
                       'name and you cannot resolve it')
    group.add_argument('-port', choices=['139', '445'], nargs='?', default='445', metavar="destination port",
                       help='Destination port to connect to SMB Server')
    group.add_argument('-cmd', action='store', metavar="cmd", help='command to execute')
    group = parser.add_argument_group('authentication')

    group.add_argument('-hashes', action="store", metavar = "LMHASH:NTHASH", help='NTLM hashes, format is LMHASH:NTHASH')
    group.add_argument('-no-pass', action="store_true", help='don\'t ask for password (useful for -k)')
    group.add_argument('-k', action="store_true", help='Use Kerberos authentication. Grabs credentials from ccache file '
                       '(KRB5CCNAME) based on target parameters. If valid credentials cannot be found, it will use the '
                       'ones specified in the command line')
    group.add_argument('-aesKey', action="store", metavar = "hex key", help='AES key to use for Kerberos Authentication '
                                                                            '(128 or 256 bits)')
    group.add_argument('-keytab', action="store", help='Read keys for SPN from keytab file')


    if len(sys.argv)==1:
        parser.print_help()
        sys.exit(1)

    options = parser.parse_args()
    # Init the example's logger theme
    logger.init(options.ts)

    if options.codec is not None:
        CODEC = options.codec
    else:
        if CODEC is None:
            CODEC = 'utf-8'

    if options.debug is True:
        logging.getLogger().setLevel(logging.DEBUG)
        # Print the Library's installation path
        logging.debug(version.getInstallationPath())
    else:
        logging.getLogger().setLevel(logging.INFO)

    import re
    domain, username, password, remoteName = re.compile('(?:(?:([^/@:]*)/)?([^@:]*)(?::([^@]*))?@)?(.*)').match(options.target).groups('')

    #In case the password contains '@'
    if '@' in remoteName:
        password = password + '@' + remoteName.rpartition('@')[0]
        remoteName = remoteName.rpartition('@')[2]

    if domain is None:
        domain = ''

    if options.keytab is not None:
        Keytab.loadKeysFromKeytab (options.keytab, username, domain, options)
        options.k = True

    if password == '' and username != '' and options.hashes is None and options.no_pass is False and options.aesKey is None:
        from getpass import getpass
        password = getpass("Password:")

    if options.target_ip is None:
        options.target_ip = remoteName

    if options.aesKey is not None:
        options.k = True

    if options.cmd is None:
        options.cmd = input("CMD> ")

    try:
        executer = LIEXEC(username, password, domain, options.hashes, options.aesKey, options.k,
                           options.dc_ip, int(options.port), options.cmd)
        executer.run(remoteName, options.target_ip)
    except Exception as e:
        if logging.getLogger().level == logging.DEBUG:
            import traceback
            traceback.print_exc()
        logging.critical(str(e))
    sys.exit(0)
